﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorio;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.ContollersTest
{
    [TestFixture]
    public class HomeTest
    {
        [Test]
        public void Caso1_RetornarLibrosIndex()
        {
            var mockrepository = new Mock<IHomeRepositorio>();
            mockrepository.Setup(o => o.TodosLibros()).Returns(new List<Libro>());
            var controller = new HomeController(mockrepository.Object);
            var view = controller.Index() as ViewResult;

            Assert.AreEqual("Index", view.ViewName);
        }
    }
}
