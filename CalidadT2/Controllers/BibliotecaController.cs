﻿using System.Linq;
using CalidadT2.Constantes;
using CalidadT2.Models;
using CalidadT2.Repositorio;
using CalidadT2.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    [Authorize]
    public class BibliotecaController : Controller
    {
        private readonly IBibliotecaRepositorio app;
        private IClaimService Iclaim;

        public BibliotecaController(IBibliotecaRepositorio app, IClaimService Iclaim)
        {
            this.app = app;
            this.Iclaim = Iclaim;
        }

        [HttpGet]
        public IActionResult Index()
        {
            Iclaim.SetHttpContext(HttpContext);
            Usuario user = Iclaim.GetLoggedUser();

            var model = app.GetBibliotecas(user.Id);

            return View("Index",model);
        }

        [HttpGet]
        public ActionResult Add(int libro)
        {
            Iclaim.SetHttpContext(HttpContext);
            Usuario user = Iclaim.GetLoggedUser();

            app.AgregarBiblioteca(libro,user.Id);

            //TempData["SuccessMessage"] = "Se añádio el libro a su biblioteca";

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult MarcarComoLeyendo(int libroId)
        {
            Iclaim.SetHttpContext(HttpContext);
            Usuario user = Iclaim.GetLoggedUser();

            var libro = app.Bibliotec(libroId, user.Id);
            app.Leer(libro);

            //TempData["SuccessMessage"] = "Se marco como leyendo el libro";

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult MarcarComoTerminado(int libroId)
        {
            Iclaim.SetHttpContext(HttpContext);
            Usuario user = Iclaim.GetLoggedUser();

            var libro = app.Bibliotec(libroId,user.Id);
            app.Culminado(libro);

            //TempData["SuccessMessage"] = "Se marco como leyendo el libro";

            return RedirectToAction("Index");
        }

        //private Usuario LoggedUser()
        //{
        //    var claim = HttpContext.User.Claims.FirstOrDefault();
        //    var user = app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
        //    return user;
        //}
    }
}
