﻿using System;
using System.Linq;
using CalidadT2.Models;
using CalidadT2.Repositorio;
using CalidadT2.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    public class LibroController : Controller
    {
        private readonly ILibroRepositorio app;
        private IClaimService Iclaim;

        public LibroController(ILibroRepositorio app, IClaimService Iclaim)
        {
            this.app = app;
            this.Iclaim = Iclaim;
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var model = app.VistaLibroDetalle(id);
            return View("Details",model);
        }

        [HttpPost]
        public IActionResult AddComentario(Comentario comentario)
        {
            Iclaim.SetHttpContext(HttpContext); 
            Usuario user = Iclaim.GetLoggedUser();

            int usuarioId = user.Id;

            app.GuardarComentario(comentario, usuarioId);

            return RedirectToAction("Details", new { id = comentario.LibroId });
        }

        //private Usuario LoggedUser()
        //{
        //    var claim = HttpContext.User.Claims.FirstOrDefault();
        //    var user = app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
        //    return user;
        //}
    }
}
