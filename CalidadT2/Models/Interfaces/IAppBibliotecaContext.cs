﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Models.Interfaces
{
    public interface IAppBibliotecaContext
    {
        DbSet<Libro> Libros { get; set; }
        DbSet<Usuario> Usuarios { get; set; }
        DbSet<Biblioteca> Bibliotecas { get; set; }
        DbSet<Comentario> Comentarios { get; set; }
        DbSet<Autor> Autores { get; set; }
        int SaveChanges();
    }
}
