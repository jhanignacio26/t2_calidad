﻿using CalidadT2.Models;
using CalidadT2.Models.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Security.Claims;

namespace CalidadT2.Service
{
    public interface IClaimService
    {
        Usuario GetLoggedUser();

        void SetHttpContext(HttpContext httpContext);

        void Logueado();

        void Login(ClaimsPrincipal principal);

    }

    public class ClaimService : IClaimService
    {
        private IAppBibliotecaContext context;
        private HttpContext httpcontext;

        public ClaimService(IAppBibliotecaContext context)
        {
            this.context = context;
        }

        public Usuario GetLoggedUser()
        {
            var claim = httpcontext.User.Claims.FirstOrDefault();
            var user = context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }

        public void Logueado()
        {
            httpcontext.SignOutAsync();
        }
        public void Login(ClaimsPrincipal principal)
        {
            httpcontext.SignInAsync(principal);
        }

        public void SetHttpContext(HttpContext httpContext)
        {
            this.httpcontext = httpContext;
        }
    }
}
